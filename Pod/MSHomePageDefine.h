//
//  MSHomePageDefine.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/5.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#ifndef MSHomePageDefine_h
#define MSHomePageDefine_h

// 详情路由
#define MSHomePageRoute_Detail @"homepage/detail"

#endif /* MSHomePageDefine_h */
