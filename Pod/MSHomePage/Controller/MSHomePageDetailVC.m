//
//  MSHomePageDetailVC.m
//  MSHomePageKit
//
//  Created by 郭明亮 on 2018/9/12.
//

#import "MSHomePageDetailVC.h"
#import "MSHomePageDefine.h"

@interface MSHomePageDetailVC ()

@end

@implementation MSHomePageDetailVC

+ (void)load {
    [MSRoute.route registerRoute:MSHomePageRoute_Detail className:NSStringFromClass(self.class)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"详情";
    self.view.backgroundColor = COLOR_RANDOM;
    
    
    UILabel *label = [[UILabel alloc]init];
    label.text = [NSString stringWithFormat:@"%ld", self.index];
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
